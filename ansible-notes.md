# Ansible

## Introduction
---

Ansible is a server provisioning tool, written in Python,  which allows you to manage a number of servers in an automated fashion. This makes the process of configuring and managing servers far simpler and more efficient. Moreover, it reduces the likelihood of making mistakes. 

For example, Ansible can be used to:

- Install, configure and deploy software
- Configure systems (with users, packages etc)
- Perform application deployment and system updates.
  
There are a few key points about Ansible to understand. 

- Ansible uses OpenSSH to connect to each server
- Ansible uses playbooks configured in YAML which act as code to provision the system
- Ansible uses the control and managed node architecture
  
This means only OpenSSH must be installed and configured on each target node (server). However, the control node must have Ansible installed. 


## Configuring Ansible
---
In order to use Ansible, and allow the control node to connect to each server, you'll need to set-up a secure public/private key-pair.

This can be done like so:
```
ssh-keygen -t ed25519 -C "ansible-keypair"
```
Next, add the public key to all target nodes. This can be done using the `ssh-copy-id` command.
```
ssh-copy-id -i ~/.ssh/ansible.pub SERVERIP
```
Using the private key, you can now use this command to login.
```
ssh -i ~/.ssh/ansiblekey SERVERIP
```
In order to test that Ansible is working, you can send ping to all servers using ansible and inventory file. This checks the connection of all servers, described in the inventory file.

The inventory file simply lists all the servers (via IP addresses or domain names that Ansible should connect to).
For example:

```Ansible
[all-hosts]
192.168.0.11
192.168.0.12
```

## Checking connections
---

We can check the remote connection using the ping module in Ansible. We specify the private key, the inventory file and which hosts to conduct the ping:
```
ansible all --key-file ~/.ssh/ansiblekey -i inventory -m ping
```

However, this command is a little long. We can reduce this by creating an `ansible.cfg` file within our working directory. Within  this file we can add our server list (group) and private key file, like so:
```bash
touch ansible.cfg

# Within ansible.cfg
[defaults]
inventory = inventory
private_key_file = ~/.ssh/ansiblekey
```

Now we can simply use:

```
ansible all -m ping
```

We can also get list of all the hosts:
```
ansible --list-hosts all

# Output
hosts (2):
  192.168.56.11
  192.168.56.12
```

# Gathering facts

Before each playbook is run, Ansible will typically 'gather facts'. This simply means that Ansible is gathering information about each server. This information can be used later in playbooks.

To gather information about each server in inventory we can use the `gather-facts` module:
```
# For all machines
ansible all -m gather_facts

# For particular machine
ansible IP -m gather_facts

#or

ansible all -m gather_facts --limit IP
```

The `--limit` options allows you to only gather information about a certain node.

## Automation
---
Let's imagine we wanted to update the cache for all our Debian servers. Doing this manually would mean logging into each server and performing the same command. This is simply impractical - imagine if we had hundreds of servers!

Ansible has a number of modules for package management, one of them is the `apt` module. 
For example, we can update the apt cache repository. However, this command requires sudo privileges.

In order to get around this, we can use the flags below `--become` & `--ask-become-pass`.
These flags allow an unprivileged user to 'become' root; the second options simply asks for the root password.

For example, we can:

- Update the apt package cache
```ansible
ansible all -m apt -a update_cache=true --become --ask-become-pass
# equivalent to apt update
```

- Install a package via the apt module
```
ansible all -m apt -a name=PACKAGENAME --become --ask-become-pass
```

- Ensure a specified package is on the latest update
```
ansible all -m apt -a "name=PACKAGENAME state=latest --become --ask-become-pass"
```
- Perform an upgrade on apt systems
```
# CHOICES=dist, fill, no (default), safe, yes
ansible all -m apt -a "upgrade=CHOICE" --become --ask-become-pass
```

Here, the `-a` flags allows us to perform ad-hoc commands; simply issuing commands as you would as if logged onto a Linux system.

## Playbooks
---

Ansible uses playbooks to automate tasks. Each playbook has a play, which in-turn has tasks. Multiple tasks can be run within a play.

Below is an example playbook to install apache2 on a node.
```yaml
---
- hosts: all
  become: true
  tasks:

  - name: update repository
    apt:
      update_cache: yes
    when: ansible_distribution = "Ubuntu"

  - name: install apache2 server
    apt:
      name: apache2
    when ansible_distribution = "Ubuntu"
```

Here, we are specifying which hosts (nodes) in the inventory file to run this playbook on and whether we should 'become' root. Next are the listed tasks to conduct on each server. 

We set a name for the tasks, and include the appropriate module name. In this case, `apt` as we want to update the repository cache on all the Debian-based machines. We set the option `update_cache` to `yes` and furthermore set a conditional. 

This is where `gathering_facts` comes into play. Ansible will procure the distribution for each node and store it. So when this playbook is executed, we can use a conditional to determine whether or not to execute a task.

We can also add multiple distributions using a Python list in the `when` condition
Let's say we had Ubuntu & Debian machines to manage (which both use apt). We can do:
  ```
  - name: update repo
    apt:
      update_cache: yes
    when: ansible_distribution in ["Debian", "Ubuntu"]
  ```

## Conclusion
---

This posts describes and explains very basic usage of Ansible, and how it works. Further and more advanced usage of Ansible will be shown in further blog posts.

However, to re-iterate:

- Ansible is a server provisioning tool
- Ansible uses OpenSSH
- Ansible uses playbooks, plays and tasks to perform automated server configuration and management.

---
- Can perform tasks before others (useful for updates) Use:
`pre_tasks`

---
### Tags
We can use tags to specify which tasks we want to run exactly. For example:

- `always` will always run the task
- Custom tags like `apache` or `ubuntu` allow you to run just those tags with this command.

```ansible

ansible-playbook --tags ubuntu tags.yml 
```
---
### Copy module
The `copy` module allows you to copy a file to the destination server. For example:

```yaml

copy:
  src: default 
  dest: /var/www/html/index.html
  owner: www
  group: www
  mode: 0644
```
**Note**: The destination file name does _not_ have to be the same as the source file name. 

---

### Unarchive
This module allows you to unarchive packages. It requires the `unzip` package be installed on the target system. Example  usage:

```yaml
- hosts: workstations
  become: true
  tags: zip
  vars: 
    terraform_link: https://releases.hashicorp.com/terraform/1.4.6/terraform_1.4.6_linux_amd64.zip
    destination: /usr/local/bin
  tasks:

    - name: Install the unzip package
      yum:
        name: unzip
    
    - name: Download the Terraform zip file.
      # Using the unarchive module.
      unarchive:
        # Using variable to set source and destination
        src: "{{ terraform_link }}" 
        dest: "{{ destination }}"
        # Remote source tells Ansible to look remotely for the source. 
        remote_src: yes
        owner: root
        group: root
        mode: 0755 
        #-rwxr-xr-x. permissions to the executable.
```
We specify the source and destination using the variables set at the start of the play.
The link to the `.zip` package is from the Hashicorp website, meaning it is a remote source, not a local one. If successful, all workstation servers specified in the inventory file will have the `terraform` binary in `/usr/local/bin`

---
### Service module
The service module allows you to manage services as you typically would with systemd.
For example, let's suppose we wanted to install the mariadb server on a CentOS machine and start the service automatically, after install. 

We first need to:

- Update the yum package cache
- Download and install the mariadb-server package
- Start and enable the service (so it starts automatically on boot)

Here is the code to do just that:

```yaml
- hosts: dbservers
  become: true
  tags: update

  pre_tasks:
    - name: Update yum package cache
      yum:
        update_cache: yes

- hosts: dbservers
  become: true
  tasks:

    - name: Install MariaDB on the database servers.
      tags: mariadb
      yum:
        name: mariadb-server 

    - name: Start MariaDB
      tags: start_mariadb
      # Using the service module
      service:
        name: mariadb
        state: started
        # Start on boot
        enabled: true
```

Here, the `service` module makes the user of:

- `name`
- `state` 
- `enabled` 

The `name` obviously specifies the service in question. The `state`  option allows us to control what state the service is in, and `enabled` starts the process automatically on reboot. When installing services on CentOS, typically the service does not automatically start, as on other distributions, like Ubuntu.

To restart a service we may do:
```yaml

 - name: Restart MariaDB
      tags: restart_maria
      service:
        name: mariadb
        state: restarted
```
The `restarted` option tells Ansible to restart the service.

--- 
### Roles

We can set-up roles by creating a `roles` directory. Within `ansible.cfg` we can set the role directory to the one we specify. For example:

```yaml
[defaults]
roles_path = ./roles
```
Now, within the current directory we can create the roles directory. Each role will have a name, for example:

- base
- webservers
- dbservers
- workstations

And within each role folder, there will be accompanying sub-folders. The main ones are:

- tasks (set of instructions for this specific)
- files (any files used within the task file)
- vars (contains any variables)

Let's start with the `base` role. Here, we shall set-up an Admin user for every server on the system. This way we can have sudo access over all servers via an ssh key imported via the role task.
For example:

```yaml
- name: Add user admin
  tags: add_admin_user
  user:
    name: admin
    groups: root
    password: $6$69c1ddc7137de84f$WXuOhSKX9MGA4WUxY2OW8RqGpn3vN89v.M979nM87jD7/6Q077b.QncJQ.RurcKjpkXFjvwNzlmnLBD7aNjAY.
    shell: /bin/sh
    
- name: Add ssh key for user admin
  authorized_key:
    user: admin
    key: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK5o2mDXy2fNqYN9RUy4ayC29RNkHJsHReOD5hbVrBzL ansible-keypair
```

Here, we create the admin user, set a password and pass the public ssh key into the authorized_keys file. Let's install apache2 for our webservers.

Within the folder `roles/webservers` we create a folder called `tasks` and `files`. Within `tasks` we create a file called `main.yml`. This will run the tasks of installing apache2 on the webservers.
For example:

```yaml
---

- name: Install apache2 on webservers (Ubuntu)
  tags: apache2,ubuntu
  apt: 
    name: apache2
    state: latest
  when: ansible_distribution == "Ubuntu"

- name: Copy dummy HTML file
  copy:
    src: dummyindex.html
    dest: /var/www/html/index.html
    owner: www-data
    group: www-data
    mode: 0644
```

Here, we are also copying a dummy `index.html` file to validate that the server is running with our own file. This file `dummyindex.html` is placed into the `files` directory. Ansible will look there when trying to copy the file to the specified destination.

Let's see how roles really work now.
Within the base directory we can create a playbook called `roles.yml`. Here, we shall specify the roles and the tasks to be conducted within those roles (which were already specified within in each task file of each role folder).

For example: 

```yaml
---

- hosts: all
  become: true
  pre_tasks: 

    - name: Update Ubuntu (apt) repo
      tags: update_ubuntu_repo
      apt:
        update_cache: true
      when: ansible_distribution== "Ubuntu"

- hosts: all
  become: true
  roles:
    - base

- hosts: webservers
  become: true
  roles: 
    - webservers

```

For simplicity, I have only assigned two roles, with simple tasks. However, within `roles.yml` we are first defining a `pre_task` which updates the apt repository (in order to get the latest packages). Secondly, the base role is being assigned to `all` servers, whereas the webservers role is being assigned to the `webservers`.

Here, Ansible will look into each role directory and run each taskbook found, using the associated `files` directory, if required. Pretty awesome!

---

## Handlers

Handlers in Ansible are used to run a task only when a change is made on a system.
For example, when a server configuration file is updated, you usually want the service to restart.
Here, handlers allow you to run a task when notified of a change.

Tasks may tell one or more handlers to execute. This accomplished via `notify`. When added to a task,
it accepts a list of handler names, which will execute when a change is detected.

CHECK EP5 MAIN.YML

---

## Modifying lines in files

You can modify files within lines using the `lineinfile` module.
Typically, it takes a destination file, and looks for the line using regular expressions.
The `line` parameter is then used to change the line to the desired output.

CHECK EP5 MAIN.YML
                                                                           
You can use `register` to store a variable within ansible. This variable is then given a name.
The `debug` module can be used to print the contents of the variable using Jinja2 syntax.

For example:

- debug: msg="Procured environment variable is {{ env_var.stdout }}"

---

## Environment

We can use the environment module for a number of purposes. For example:

We can use it to go through a http(s) proxy.

```yaml

tasks: 
  - name: Download a file.
    get_url: 
      url: test.com/file.zip
      dest: /tmp
    environment:
      http_proxy: http://example-proxy:80
      https_proxy: https://example-proxy:80 
```

