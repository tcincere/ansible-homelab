# Ansible Homelab

## Description
A Gitlab repository to demonstrate and practice Ansible within a homelab environment.
The main workstation uses CentOS7 and the two servers are running Ubuntu 18.04 Bionic. 
The virtual machines were created and configured using Terraform's Vagrant software.
